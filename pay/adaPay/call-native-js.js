// Function to detect if the environment is iOS
function isIOS() {
    return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
}

pGetPlantform = function () {
    var plantform = '',
        userAgent = navigator.userAgent;
    if (isIOS()) {  //iPhone|iPad|iPod|iOS
        plantform = 'IOS';
    } else if (/(Android)/i.test(userAgent)) {   //Android
        plantform = 'ANDROID';
    } else {
        plantform = 'PC';
    }
    return plantform;
}

function callNativeApp(methodName, data) {
    console.log('methodName',methodName)
    var plantform = pGetPlantform();
    if (plantform === 'IOS') {
        if (data == null) {
            window.webkit.messageHandlers[methodName].postMessage();
        } else {
            window.webkit.messageHandlers[methodName].postMessage(data);
        }
    } else if (plantform === 'ANDROID') {
        if (data == null) {
            // console.log('window.xinpai[methodName]',JSON.stringify(window.xinpai[methodName]),window.xinpai)
            window.xinpai.finishMobile();
        } else {
            window.xinpai.finishMobile(data);
        }

    }
}
