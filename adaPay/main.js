// $(function(){
//     if (/MicroMessenger/.test(window.navigator.userAgent)) { 
//         //alert('微信客户端'); 
//     } else if (/AlipayClient/.test(window.navigator.userAgent)) { 
//         //alert('支付宝客户端');
//     } else {
//         //alert('其他浏览器');
//     }
// })

function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
    return decodeURI(r[2]);
    return null;
}


function alertMessage(msg, type, data) {
    var $alert = $('.alert-filter1').find('.ptext'),
        type = type || 1,
        data = data || {};
    if (type == 1) {
        $(".alert-filter1 .hidden").hide();
        $(".alert-filter1").show();
        $(".alert-filter1 p").removeClass('alert-dialog');
        $alert.html(msg);
        setTimeout(function () {
            $(".alert-filter1").fadeOut(600)
        }, 3000)
    }
}


/**
 *  转换金额为保留2位小数
 * @param x
 * @returns 强制保留2位小数的金额
 */
function changeTwoDecimal(amt) {
    var f_x = parseFloat(amt);
    if (isNaN(f_x)) {
        return "0.00";
    }
    var f_x = Math.round(f_x * 100) / 100;
    var s_x = f_x.toString();
    var pos_decimal = s_x.indexOf('.');
    if (pos_decimal < 0) {
        pos_decimal = s_x.length;
        s_x += '.';
    }
    while (s_x.length <= pos_decimal + 2) {
        s_x += '0';
    }
    return s_x;
}

function goAuth() {
      let urlNow=encodeURIComponent(window.location.href);
      let scope='snsapi_base';    //snsapi_userinfo   //静默授权 用户无感知
      let appid='wx5ed40636c41bdf76';
      let url=`https://open.weixin.qq.com/connect/oauth2/authorize?appid=${appid}&redirect_uri=${urlNow}&response_type=code&scope=${scope}&state=STATE&connect_redirect=1#wechat_redirect`;
      window.location.replace(url);
}